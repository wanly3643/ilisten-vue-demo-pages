import { createApp } from 'vue'
import { createI18n } from 'vue-i18n'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './style.css'
import App from './App.vue'

const messages = {
    en: {
        message: {
            dropFileOr: 'Drop file here or ',
            clickSelect: 'click to select',
            hideScript: 'Hide Script',
            onlyMarked: 'Only Marked',
            selectScript: 'Select Script',
            selectAudio: 'Select Audio',
            pauseAudio: 'Pause',
            playAudi: 'Play'
        }
    },
    zh: {
        message: {
            dropFileOr: '拖拽文件或',
            clickSelect: '点击选择',
            hideScript: '隐藏字幕',
            onlyMarked: '只显示标记字幕',
            selectScript: '选择字幕',
            selectAudio: '选择音频',
            pauseAudio: '暂停',
            playAudi: '播放'
        }
    }
}

// 2. Create i18n instance with options
const i18n = createI18n({
    locale: 'zh', // set locale
    fallbackLocale: 'en', // set fallback locale
    messages, // set locale messages
    // If you need to specify other options, you can set other options
    // ...
})

const app = createApp(App)
app.use(ElementPlus)
app.use(i18n)
app.mount('#app')
